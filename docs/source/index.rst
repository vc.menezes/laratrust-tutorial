Laratrust Docs
==============

.. raw:: html

    <p style="text-align: center;">

.. image:: https://i.imgur.com/b1bs9Wcg.jpg

.. raw:: html

    </p>

Table of Contents:
==================
.. NOTE::
    Please read all the sections in order.

.. toctree::

    upgrade
    installation
    configuration/index
    usage/index
    troubleshooting
    license
    contributing
