<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\User;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Role $role)
    {
        $roles = $role->all();
        return response()->json($roles, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Role $role, Request $request)
    {
        $params = $request->only('name', 'display_name', 'description');
        $roles = $role->create($params);
        return response()->json($roles, 200);
    }

    public function attachRole(User $user, Request $request, Role $role)
    {
        $users = $user->find($request->user);
        $roles = $role->find($request->role);

        // equivalent to $user->roles()->attach([$admin->id]);
        $users->attachRole($roles);
        return response()->json([
            'message' => 'attached']
        , 200);
    }

    public function removeRole(User $user, Request $request, Role $role)
    {
        $users = $user->find($request->user);
        $roles = $role->find($request->role);

        // equivalent to $user->roles()->detach([$admin->id]);
        $users->detachRole($roles);
        return response()->json([
            'message' => 'detach']
        , 200);
    }
}
