<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class RedisController extends Controller
{
    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        echo 'test';
    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function show(Redis $redis)
    {
        $counter = $redis->get('keyname');
        echo $counter;
    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function download(Redis $redis)
    {
        $counter = $redis->incr('keyna');
        echo $counter;
    }

      /**
     * [index description]
     * @return [type] [description]
     */
    public function showBlog(Redis $redis, $id)
    {
        $key = "blog:{$id}:views";

        $views = $redis->get($key);

        echo "No of views for blog {$id}: {$views}";
    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function viewBlog(Redis $redis, $id)
    {
        $key = "blog:{$id}:views";

        $redis->incr($key);
    }
}
