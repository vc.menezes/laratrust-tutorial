<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;

class AuthController extends Controller
{
    /**
     * Login user
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token'));
    }

    public function getInfo()
    {
        $users = auth()->user();
        $users['role'] = auth()->user()->roles()->first();
        return response()->json($users, 200);

    }

    public function logout()
    {
        $logout = JWTAuth::invalidate(JWTAuth::getToken());
        if ($logout === true) {
            return response()->json(true, 200);
        }
        return response()->json(['message' => 'invalid_token'], 422);
    }
}
