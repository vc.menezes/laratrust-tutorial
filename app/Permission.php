<?php

namespace App;

use Laratrust\LaratrustPermission;

class Permission extends LaratrustPermission
{
    /**
     * Table related to the model
     * @var string
     */
    protected $table = 'permissions';

    /**
     * Mass assignable fields to model
     * @var [type]
     */
    protected $fillable = [
        'name',
        'display_name',
        'description'
    ];
}
