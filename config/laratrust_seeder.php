<?php

return [
    'role_structure' => [
        'superadministrator' => [
            'users' => 'c,r,u,d',
            'acl' => 'c,r,u,d',
            'announcement' => 'c,r,u,d',
            'profile' => 'r,u',
            'role' => 'c,r,u,d',
            'permission'=> 'c,r,u,d',
        ],
        'secretary' => [
            'users' => 'c,r,u,d',
            'announcement' => 'c,r,u',
            'profile' => 'r,u',
            'role' => 'c,r,u',
            'permission'=> 'c,r,u',
        ],
        'user' => [
            'profile' => 'r,u',
            'announcement' => 'c,r',
            'role' => 'r',
            'permission'=> 'r',
        ],
    ],
    'permission_structure' => [
        'cru_user' => [
            'profile' => 'c,r,u'
        ],
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
