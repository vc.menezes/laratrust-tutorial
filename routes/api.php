<?php


//Authenticate user
Route::post('authenticate', 'AuthController@authenticate');

Route::group(['middleware' => 'jwt.auth'], function() {
    //Logout
    Route::post('logout', 'AuthController@logout');

    //Get authenticated user info
    Route::get('/me', ['middleware' => 'jwt.auth', 'uses' => 'AuthController@getInfo']);

    /**
     * Routes for users
     *
     */
    Route::group(['prefix' => 'users'], function(){
        Route::get('/', 'UserController@index');
        Route::get('/{user}', 'UserController@show');
        Route::post('/', ['middleware' => ['ability:superadministrator|secretary,create-users|create-users'], 'uses' => 'UserController@store']);
        Route::put('/{user}', ['middleware' => ['ability:superadministrator|secretary,update-users|update-users'], 'uses' => 'UserController@update']);
        Route::delete('/{user}', ['middleware' => ['ability:superadministrator,delete-users'], 'uses' => 'UserController@destroy']);
    });

    Route::group(['prefix' => 'roles'], function(){
        Route::get('/', 'RoleController@index');
        Route::post('/', ['middleware' => ['ability:superadministrator|secretary,create-role|create-role'], 'uses' => 'RoleController@store']);
        Route::post('/attach', ['middleware' => ['ability:superadministrator|secretary,update-role|update-role'], 'uses' => 'RoleController@attachRole']);
        Route::post('/detach', ['middleware' => ['ability:superadministrator|secretary,delete-role|delete-role'], 'uses' => 'RoleController@removeRole']);
    });

    Route::group(['prefix' => 'permissions'], function(){
        Route::get('/', 'PermissionController@index');
        Route::post('/', ['middleware' => ['ability:superadministrator|secretary,create-permission|create-permission'], 'uses' => 'PermissionController@store']);
        Route::post('/attach', ['middleware' => ['ability:superadministrator|secretary,update-permission|update-permission'], 'uses' => 'PermissionController@attachPermission']);
        Route::post('/detach', ['middleware' => ['ability:superadministrator|secretary,delete-permission|delete-permission'], 'uses' => 'PermissionController@removePermission']);
    });

});